package tk.shanebee.ag.commands;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import tk.shanebee.ag.AG;
import tk.shanebee.ag.util.Util;

public abstract class BaseCmd {

	public CommandSender sender;
	public String[] args;
	public String cmdName;
	public int argLength = 0;
	public boolean forcePlayer = true;
	public boolean forceInGame = false;
	public boolean forceInRegion = false;
	public String usage = "";
	public Player player;

	public boolean processCmd(AG p, CommandSender s, String[] arg) {
		sender = s;
		args = arg;

		if (forcePlayer) {
			if (!(s instanceof Player)) return false;
			else player = (Player) s;
		}
		if (!s.hasPermission("ag." + cmdName))
			Util.scm(sender, AG.getPlugin().getLang().cmd_base_noperm.replace("<command>", cmdName));
		else if (forceInGame && !AG.getPlugin().getPlayers().containsKey(player.getUniqueId()) && !AG.getPlugin().getSpectators().containsKey(player.getUniqueId()))
			Util.scm(sender, AG.getPlugin().getLang().cmd_base_nogame);
		else if (forceInRegion && !AG.getPlugin().getManager().isInRegion(player.getLocation()))
			Util.scm(sender, AG.getPlugin().getLang().cmd_base_noregion);
		else if (argLength > arg.length)
			Util.scm(s, AG.getPlugin().getLang().cmd_base_wrongusage + " " + sendHelpLine());
		else return run();
		return true;
	}

	public abstract boolean run();

	public String sendHelpLine() {
		return "&3&l/ag &b" + cmdName + " &6" + usage.replaceAll("<", "&7&l<&f").replaceAll(">", "&7&l>");
	}
}
