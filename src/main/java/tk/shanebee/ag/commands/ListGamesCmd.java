package tk.shanebee.ag.commands;

import tk.shanebee.ag.game.Game;
import tk.shanebee.ag.AG;
import tk.shanebee.ag.util.Util;

public class ListGamesCmd extends BaseCmd {

	public ListGamesCmd() {
		forcePlayer = false;
		cmdName = "listgames";
		forceInGame = false;
		argLength = 1;
	}

	@Override
	public boolean run() {
		Util.scm(sender, "&6&l Games:");
		for (Game g : AG.getPlugin().getGames()) {
			Util.scm(sender, " &4 - &6" + g.getName() + "&4:&6" + g.getStatus().getName());
		}
		return true;
	}
}