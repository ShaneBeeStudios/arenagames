package tk.shanebee.ag.commands;

import tk.shanebee.ag.game.Game;
import tk.shanebee.ag.AG;
import tk.shanebee.ag.Status;
import tk.shanebee.ag.util.Util;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class DeleteCmd extends BaseCmd {

	public DeleteCmd() {
		forcePlayer = false;
		cmdName = "delete";
		forceInGame = false;
		argLength = 2;
		usage = "<arena-name>";
	}

	@Override
	public boolean run() {
		Game g = AG.getPlugin().getManager().getGame(args[1]);
		if (g != null) {
			try {
				Util.scm(sender, AG.getPlugin().getLang().cmd_delete_attempt.replace("<arena>", g.getName()));

				if (g.getStatus() == Status.BEGINNING || g.getStatus() == Status.RUNNING) {
					Util.scm(sender, "  &7- &cGame running! &aStopping..");
					g.forceRollback();
					g.stop(false);
				}
				if (!g.getPlayers().isEmpty()) {
					Util.scm(sender, AG.getPlugin().getLang().cmd_delete_kicking);
					for (UUID u : g.getPlayers()) {
						Player p = Bukkit.getPlayer(u);
						if (p != null) {
							g.leave(p, false);
						}
					}
				}
				AG.getPlugin().getArenaConfig().getCustomConfig().set("arenas." + args[1], null);
				AG.getPlugin().getArenaConfig().saveCustomConfig();
				Util.scm(sender, AG.getPlugin().getLang().cmd_delete_deleted.replace("<arena>", g.getName()));
				AG.getPlugin().getGames().remove(g);
			} catch (Exception e) {
				Util.scm(sender, AG.getPlugin().getLang().cmd_delete_failed);
			}
		} else {
			Util.scm(sender, AG.getPlugin().getLang().cmd_delete_noexist);
		}
		return true;
	}
}