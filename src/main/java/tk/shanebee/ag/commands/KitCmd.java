package tk.shanebee.ag.commands;

import tk.shanebee.ag.game.Game;
import tk.shanebee.ag.util.Util;
import tk.shanebee.ag.AG;
import tk.shanebee.ag.Status;
public class KitCmd extends BaseCmd {

	public KitCmd() {
		forcePlayer = true;
		cmdName = "kit";
		forceInGame = true;
		argLength = 2;
		usage = "<kit>";
	}

	@Override
	public boolean run() {
		Game game = AG.getPlugin().getPlayers().get(player.getUniqueId()).getGame();
		Status st = game.getStatus();
		if (st == Status.WAITING || st == Status.COUNTDOWN) {
			game.getKitManager().setKit(player, args[1]);
		} else {
			Util.scm(player, AG.getPlugin().getLang().cmd_kit_no_change);
		}
		return true;
	}
}