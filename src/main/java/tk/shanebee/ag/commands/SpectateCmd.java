package tk.shanebee.ag.commands;

import tk.shanebee.ag.game.Game;
import tk.shanebee.ag.AG;
import tk.shanebee.ag.Status;
import tk.shanebee.ag.util.Util;

public class SpectateCmd extends BaseCmd {

	public SpectateCmd() {
		forcePlayer = true;
		cmdName = "spectate";
		forceInGame = false;
		argLength = 2;
	}

	@Override
	public boolean run() {
		if (AG.getPlugin().getPlayers().containsKey(player.getUniqueId()) || AG.getPlugin().getSpectators().containsKey(player.getUniqueId())) {
			Util.scm(player, AG.getPlugin().getLang().cmd_join_in_game);
		} else {
			Game game = AG.getPlugin().getManager().getGame(args[1]);
			if (game != null && !game.getPlayers().contains(player.getUniqueId()) && !game.getSpectators().contains(player)) {
				Status status = game.getStatus();
				if (status == Status.RUNNING || status == Status.BEGINNING) {
					game.spectate(player);
				} else {
					Util.scm(player, "This game is not running, status: " + status);
				}
			} else {
				Util.scm(player, AG.getPlugin().getLang().cmd_delete_noexist);
			}
		}
		return true;
	}

}
