package tk.shanebee.ag.commands;

import tk.shanebee.ag.*;
import tk.shanebee.ag.data.Config;
import tk.shanebee.ag.game.Game;
import tk.shanebee.ag.util.Util;
import tk.shanebee.ag.util.Vault;

public class LeaveCmd extends BaseCmd {

	public LeaveCmd() {
		forcePlayer = true;
		cmdName = "leave";
		forceInGame = true;
		argLength = 1;
	}

	@Override
	public boolean run() {
		Game game;
		if (AG.getPlugin().getPlayers().containsKey(player.getUniqueId())) {
			game = AG.getPlugin().getPlayers().get(player.getUniqueId()).getGame();
			if (Config.economy) {
				Status status = game.getStatus();
				if ((status == Status.WAITING || status == Status.COUNTDOWN) && game.getCost() > 0) {
					Vault.economy.depositPlayer(player, game.getCost());
					Util.scm(player, AG.getPlugin().getLang().prefix +
							AG.getPlugin().getLang().cmd_leave_refund.replace("<cost>", String.valueOf(game.getCost())));
				}
			}
			game.leave(player, false);
		} else {
			game = AG.getPlugin().getSpectators().get(player.getUniqueId()).getGame();
			game.leaveSpectate(player);
		}
		Util.scm(player, AG.getPlugin().getLang().prefix + AG.getPlugin().getLang().cmd_leave_left.replace("<arena>", game.getName()));
		return true;
	}
}