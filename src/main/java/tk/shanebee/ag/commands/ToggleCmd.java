package tk.shanebee.ag.commands;

import tk.shanebee.ag.game.Game;
import tk.shanebee.ag.AG;
import tk.shanebee.ag.Status;
import tk.shanebee.ag.util.Util;

public class ToggleCmd extends BaseCmd {

	public ToggleCmd() {
		forcePlayer = false;
		cmdName = "toggle";
		forceInGame = false;
		argLength = 2;
		usage = "<game>";
	}

	@Override
	public boolean run() {
		Game g = AG.getPlugin().getManager().getGame(args[1]);
		if (g != null) {
			if (g.getStatus() == Status.NOTREADY || g.getStatus() == Status.BROKEN) {
				g.setStatus(Status.READY);
				Util.scm(sender, AG.getPlugin().getLang().cmd_toggle_unlocked.replace("<arena>", g.getName()));
			} else {
				g.stop(false);
				g.setStatus(Status.NOTREADY);
				Util.scm(sender, AG.getPlugin().getLang().cmd_toggle_locked.replace("<arena>", g.getName()));
			}
		} else {
			Util.scm(sender, AG.getPlugin().getLang().cmd_delete_noexist);
		}
		return true;
	}

}