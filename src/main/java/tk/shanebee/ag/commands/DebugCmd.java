package tk.shanebee.ag.commands;

import tk.shanebee.ag.AG;

public class DebugCmd extends BaseCmd {

	public DebugCmd() {
		forcePlayer = false;
		cmdName = "debug";
		forceInGame = false;
		argLength = 2;
		usage = "<game>";
	}

	@Override
	public boolean run() {
			AG.getPlugin().getManager().runDebugger(sender, args[1]);
		return true;
	}
}