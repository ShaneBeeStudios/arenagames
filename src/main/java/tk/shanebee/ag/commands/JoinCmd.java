package tk.shanebee.ag.commands;

import tk.shanebee.ag.*;
import tk.shanebee.ag.data.Config;
import tk.shanebee.ag.game.Game;
import tk.shanebee.ag.util.Util;
import tk.shanebee.ag.util.Vault;

public class JoinCmd extends BaseCmd {

	public JoinCmd() {
		forcePlayer = true;
		cmdName = "join";
		forceInGame = false;
		argLength = 2;
		usage = "<arena-name>";
	}

	@Override
	public boolean run() {

		if (AG.getPlugin().getPlayers().containsKey(player.getUniqueId())) {
			Util.scm(player, AG.getPlugin().getLang().cmd_join_in_game);
		} else {
			Game g = AG.getPlugin().getManager().getGame(args[1]);
			if (g != null && !g.getPlayers().contains(player.getUniqueId())) {
				if (Config.economy) {
					if (Vault.economy.getBalance(player) >= g.getCost()) {
						Vault.economy.withdrawPlayer(player, g.getCost());
						g.join(player);
					} else {
						Util.scm(player, AG.getPlugin().getLang().prefix +
								AG.getPlugin().getLang().cmd_join_no_money.replace("<cost>", String.valueOf(g.getCost())));
					}
				} else {
					g.join(player);
				}
			} else {
				Util.scm(player, AG.getPlugin().getLang().cmd_delete_noexist);
			}
		}
		return true;
	}
}