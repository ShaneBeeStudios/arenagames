package tk.shanebee.ag.commands;

import tk.shanebee.ag.game.Game;
import tk.shanebee.ag.AG;
import tk.shanebee.ag.util.Util;

import org.bukkit.Location;

public class SetExitCmd extends BaseCmd {

	public SetExitCmd() {
		forcePlayer = true;
		cmdName = "setexit";
		forceInGame = false;
		argLength = 1;
	}

	@Override
	public boolean run() {
		Location l = player.getLocation();
		String loc = l.getWorld().getName() + ":" + l.getBlockX() + ":" + l.getBlockY() + ":" + l.getBlockZ() + ":" + l.getYaw() + ":" + l.getPitch();
		AG.getPlugin().getConfig().set("settings.globalexit", loc);
		AG.getPlugin().saveConfig();
		Util.scm(player, AG.getPlugin().getLang().cmd_exit_set + " " + loc.replace(":", "&6,&c "));
		
		for (Game g : AG.getPlugin().getGames())
			g.setExit(l);
		return true;
	}
}