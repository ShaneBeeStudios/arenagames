package tk.shanebee.ag.commands;

import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;

import tk.shanebee.ag.game.Game;
import tk.shanebee.ag.AG;
import tk.shanebee.ag.util.Util;

public class SetLobbyWallCmd extends BaseCmd {

	public SetLobbyWallCmd() {
		forcePlayer = true;
		cmdName = "setlobbywall";
		forceInGame = false;
		argLength = 2;
		usage = "<arena-name>";
	}

	@Override
	public boolean run() {
		Game g = AG.getPlugin().getManager().getGame(args[1]);
		if (g != null) {
			Block b = player.getTargetBlockExact(6);
			if (b !=  null && Util.isWallSign(b.getType()) && g.setLobbyBlock((Sign)b.getState())) {
				Location l = b.getLocation();
				assert l.getWorld() != null;
				AG.getPlugin().getArenaConfig().getCustomConfig().set(("arenas." + args[1] + "." + "lobbysign"), (l.getWorld().getName() + ":" + l.getBlockX() + ":" + l.getBlockY() + ":" + l.getBlockZ()));
				AG.getPlugin().getArenaConfig().saveCustomConfig();
				Util.scm(player, AG.getPlugin().getLang().cmd_lobbywall_set);
				AG.getPlugin().getManager().checkGame(g, player);
			} else {
				Util.scm(player, AG.getPlugin().getLang().cmd_lobbywall_notcorrect);
				Util.scm(player, AG.getPlugin().getLang().cmd_lobbywall_format);
			}
		} else {
			player.sendMessage(AG.getPlugin().getLang().cmd_delete_noexist);
		}
		return true;
	}
}