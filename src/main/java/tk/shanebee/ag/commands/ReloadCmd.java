package tk.shanebee.ag.commands;

import tk.shanebee.ag.data.Config;
import tk.shanebee.ag.AG;
import tk.shanebee.ag.util.Util;

public class ReloadCmd extends BaseCmd {

	public ReloadCmd() {
		forcePlayer = true;
		cmdName = "reload";
		argLength = 1;
		forceInRegion = false;
	}

	@Override
	public boolean run() {
		Util.scm(player, AG.getPlugin().getLang().cmd_reload_attempt);
		AG.getPlugin().stopAll();
		//AG.getPlugin().getArenaConfig().saveCustomConfig();
		AG.getPlugin().getArenaConfig().reloadCustomConfig();
		AG.getPlugin().getArenaConfig().load();
		Util.scm(player, AG.getPlugin().getLang().cmd_reload_reloaded_arena);
		AG.getPlugin().getKitManager().getKits().clear();
		AG.getPlugin().getItemStackManager().setKits();
		Util.scm(player, AG.getPlugin().getLang().cmd_reload_reloaded_kit);
		AG.getPlugin().getItems().clear();
		AG.getPlugin().getRandomItems().load();
		Util.scm(player, AG.getPlugin().getLang().cmd_reload_reloaded_items);
		new Config(AG.getPlugin());
		Util.scm(player, AG.getPlugin().getLang().cmd_reload_reloaded_config);

		Util.scm(player, AG.getPlugin().getLang().cmd_reload_reloaded_success);
		return true;
	}

}
