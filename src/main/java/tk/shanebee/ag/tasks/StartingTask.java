package tk.shanebee.ag.tasks;

import org.bukkit.Bukkit;

import tk.shanebee.ag.game.Game;
import tk.shanebee.ag.AG;
import tk.shanebee.ag.util.Util;

public class StartingTask implements Runnable {

	private int timer;
	private int id;
	private Game game;

	public StartingTask(Game g) {
		this.timer = 30;
		this.game = g;
		Util.broadcast(AG.getPlugin().getLang().game_started.replace("<arena>", g.getName()));
		Util.broadcast(AG.getPlugin().getLang().game_join.replace("<arena>", g.getName()));

		this.id = Bukkit.getScheduler().scheduleSyncRepeatingTask(AG.getPlugin(), this, 5 * 20L, 5 * 20L);
	}

	@Override
	public void run() {
		timer = (timer - 5);

		if (timer <= 0) {
			stop();
			game.startFreeRoam();
		} else {
			game.msgAll(AG.getPlugin().getLang().game_countdown.replace("<timer>", String.valueOf(timer)));
		}
	}

	public void stop() {
		Bukkit.getScheduler().cancelTask(id);
	}
}
