package tk.shanebee.ag.tasks;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import tk.shanebee.ag.game.Game;
import tk.shanebee.ag.AG;
import tk.shanebee.ag.util.Util;

public class FreeRoamTask implements Runnable {

	private Game game;
	private int id;

	public FreeRoamTask(Game g) {
		this.game = g;
		for (UUID u : g.getPlayers()) {
			Player p = Bukkit.getPlayer(u);
			if (p != null) {
				Util.scm(p, AG.getPlugin().getLang().roam_game_started);
				Util.scm(p, AG.getPlugin().getLang().roam_time.replace("<roam>", String.valueOf(g.getRoamTime())));
				p.setHealth(20);
				p.setFoodLevel(20);
				g.unFreeze(p);
			}
		}
		this.id = Bukkit.getScheduler().scheduleSyncDelayedTask(AG.getPlugin(), this, g.getRoamTime() * 20L);
	}

	@Override
	public void run() {
		game.msgAll(AG.getPlugin().getLang().roam_finished);
		game.startGame();
	}

	public void stop() {
		Bukkit.getScheduler().cancelTask(id);
	}
}
