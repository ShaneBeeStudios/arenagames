package tk.shanebee.ag.listeners;

import tk.shanebee.ag.AG;
import tk.shanebee.ag.Status;
import tk.shanebee.ag.util.Util;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

/**
 * Internal event listener
 */
public class CancelListener implements Listener {

	public AG plugin;

	public CancelListener(AG instance) {
		plugin = instance;
	}

	@EventHandler(priority=EventPriority.LOWEST)
	private void onPlayerCommandPreprocess(PlayerCommandPreprocessEvent event) {
		Player player = event.getPlayer();
		String[] st = event.getMessage().split(" ");
		if ((plugin.getPlayers().containsKey(player.getUniqueId()) || plugin.getSpectators().containsKey(player.getUniqueId())) &&
				!st[0].equalsIgnoreCase("/login")) {
			if (st[0].equalsIgnoreCase("/ag")) {
				if (st.length >= 2 && st[1].equalsIgnoreCase("kit") &&
						plugin.getPlayers().get(player.getUniqueId()).getGame().getStatus() == Status.RUNNING) {
					event.setMessage("/");
					event.setCancelled(true);
					Util.scm(player, AG.getPlugin().getLang().cmd_handler_nokit);
				}
				return;
			}
			event.setMessage("/");
			event.setCancelled(true);
			Util.scm(player, AG.getPlugin().getLang().cmd_handler_nocmd);
		} else if ("/tp".equalsIgnoreCase(st[0]) && st.length >= 2) {
			Player p = Bukkit.getServer().getPlayer(st[1]);
			if (p != null) {
				if (plugin.getPlayers().containsKey(p.getUniqueId())) {
					Util.scm(player, AG.getPlugin().getLang().cmd_handler_playing);
					event.setMessage("/");
					event.setCancelled(true);
				}
			}
		} 
	}
}
