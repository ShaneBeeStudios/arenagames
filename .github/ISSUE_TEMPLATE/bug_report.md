---
name: Bug report
about: Create a report to help us improve
title: "[BUG]"
labels: ''
assignees: ''

---

**Describe the bug**
A clear and concise description of what the bug is.

**To Reproduce**
Give a detailed description of how to reproduce this issue

**Expected behavior**
A clear and concise description of what you expected to happen.

**Screenshots**
If applicable, add screenshots to help explain your problem.

**Environment Information (please complete the following information):**
 - CraftButkkit/Spigot/Paper version: **-use /ver to find this info-**
 - HungerGames version: **-use /ver hungergames  to find this info-**

**Additional context**
Add any other context about the problem here.
